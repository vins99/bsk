package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.*;


public class FrameTest {

	@Test
	public void testFirstThrowOfFrame() throws Exception{
		Frame frame = new Frame(2,4);
		assertEquals(2,frame.getFirstThrow());
	}

	@Test
	public void testSecondThrowOfFrame() throws Exception{
		Frame frame = new Frame(2,4);
		assertEquals(4,frame.getSecondThrow());
	}
	
	@Test
	public void testScore() throws Exception{
		Frame frame = new Frame(2,4);
		assertEquals(6,frame.getScore());
	}
	
	
	
}
