package tdd.training.bsk;
	
public class Frame {

	private int FirstThrow = 0;
	private int SecondThrow = 0;
	private int Bonus = 0;
	private int Score = 0;
	
	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		
		FirstThrow = firstThrow;
		SecondThrow = secondThrow;
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		
		return FirstThrow;
		
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		
		return SecondThrow;
		
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		
		Bonus = bonus;
		
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		
		return Bonus;
		
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
		
		if(isStrike() == true) {
			
			Score = getFirstThrow() + getBonus();
			
		}else if(isSpare() == true) {
			
			Score = getFirstThrow() + getSecondThrow() + getBonus();
			
		}else {
			
			Score = getFirstThrow() + getSecondThrow();
			
		}
		
		return Score;
		
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		
		return ((getFirstThrow() == 10 && getSecondThrow() == 0));
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		
		return (getFirstThrow() !=10 && getSecondThrow() !=10 &&  getFirstThrow() + getSecondThrow() == 10);
	}

}
