package tdd.training.bsk;

public class Game {

	private Frame game[] = new Frame[10];
	private int counter = 0;
	private int firstBonus = 0;
	private int secondBonus = 0;
	
	/**
	 * It initializes an empty bowling game.
	 * @throws BowlingException 
	 */
	public Game() throws BowlingException {
		
		for(int i=0; i<10; i++) {
			
			game[i] = new Frame(0,0);
			
		}
		
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		
		game[counter] = frame;
		counter++;
		
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		
		return game[index];
				
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		
		firstBonus = firstBonusThrow;
		
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		
		secondBonus = secondBonusThrow;
		
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		
		return firstBonus;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		
		return secondBonus;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		
		int sum = 0;
		boolean Spare = false;
		boolean Strike = false;
		int Bonus = 0;
		int NumberOfStrike = 0;
		
		for(int i=0; i<10; i++) {
			
			sum += game[i].getScore();
			if(i<9) {
				
			
			if(Spare == true) {
				
				game[i-1].setBonus(game[i].getFirstThrow());
				sum += game[i-1].getBonus();
				Spare = false;
			}
				
				if(Strike == true) {
					game[i-1].setBonus(game[i].getScore());
					Bonus = game[i-1].getBonus();
					if(game[i].isStrike()) {
						
						Bonus = 10 + game[i+1].getFirstThrow();
						game[i].setBonus(Bonus);
						
					}
					
						sum += Bonus; 
						
						Strike = false;
				}
					
				if(game[i].isSpare()) {
					
					Spare = true;
					
				}
				
				if(game[i].isStrike()) {
					
					Strike = true;
					NumberOfStrike++;
					
				}
			}else {
				
				if(game[i].isSpare()) {
					
					sum += getFirstBonusThrow();
					
				}
				
				if(game[i].isStrike()) {
					
					NumberOfStrike++;
					if(getFirstBonusThrow()+ getSecondBonusThrow() == 20) {
						
						NumberOfStrike += 2;
						
					}
					sum += getFirstBonusThrow() + getSecondBonusThrow();
					
					
				}
			}
			
		}
			
		if(NumberOfStrike == 12) {
			
			return 300;
			
		}
		
		return sum;
		
	}

}
